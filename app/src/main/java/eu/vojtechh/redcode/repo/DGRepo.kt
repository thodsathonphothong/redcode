package eu.vojtechh.redcode.repo

import eu.vojtechh.redcode.model.Album
import eu.vojtechh.redcode.net.discogs.DGApiHelper
import eu.vojtechh.redcode.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class DGRepo @Inject constructor(private val dgApiHelper: DGApiHelper) {
    fun searchBarcode(barcode: String) = flow {
        emit(Resource.loading("Loading info from Discogs"))

        val auth = "Discogs token=ENTER TOKEN HERE"
        val searchResponse = dgApiHelper.searchBarcode(auth, barcode)

        if (!searchResponse.isSuccessful) {
            emit(Resource.error(searchResponse.errorBody().toString(), null))
            return@flow
        }

        val searchResults = searchResponse.body()?.results
        if (searchResults.isNullOrEmpty()) {
            emit(Resource.error("No barcodes found on discogs.", null))
            return@flow
        }
        emit(Resource.loading("Searching for master releases"))
        val master = searchResults.find { it.master_id != null && it.master_id != "0"}
        if (master != null) {
            val masterReleaseResponse = dgApiHelper.getMaster(auth, master.master_id!!)
            if (!masterReleaseResponse.isSuccessful) {
                emit(Resource.error(masterReleaseResponse.errorBody().toString(), null))
                return@flow
            }
            masterReleaseResponse.body()?.let {
                if (it.artists.isNullOrEmpty()) {
                    emit(
                        Resource.error(
                            "No artists for master release found od discogs.",
                            null
                        )
                    )
                    return@flow
                }
                val artist = it.artists.first()
                emit(Resource.success(Album(artist.name, it.title, it.year)))
                return@flow
            }
        } else {
            emit(Resource.loading("Searching for any releases"))
            val release = searchResults.find { it.release_id != null }
            if (release != null) {

                val releaseResponse = dgApiHelper.getRelease(auth, release.release_id!!)
                if (!releaseResponse.isSuccessful) {
                    emit(Resource.error(releaseResponse.errorBody().toString(), null))
                    return@flow
                }
                releaseResponse.body()?.let {
                    if (it.artists.isNullOrEmpty()) {
                        emit(Resource.error("No artists for release found od discogs.", null))
                        return@flow
                    }
                    val artist = it.artists.first()
                    emit(Resource.success(Album(artist.name, it.title, it.year)))
                    return@flow
                }
            } else {
                emit(Resource.error("No valid items found od discogs.", null))
                return@flow
            }
        }
    }.flowOn(Dispatchers.IO)
}
