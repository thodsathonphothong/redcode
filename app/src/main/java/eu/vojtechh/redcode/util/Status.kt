package eu.vojtechh.redcode.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}