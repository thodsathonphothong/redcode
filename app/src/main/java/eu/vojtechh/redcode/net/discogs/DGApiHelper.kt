package eu.vojtechh.redcode.net.discogs

import eu.vojtechh.redcode.model.discogs.DGAlbum
import eu.vojtechh.redcode.model.discogs.DGResponse
import retrofit2.Response

interface DGApiHelper {
    suspend fun searchBarcode(auth: String, barcode: String): Response<DGResponse>
    suspend fun getMaster(auth: String, masterId: String): Response<DGAlbum>
    suspend fun getRelease(auth: String, releaseId: String): Response<DGAlbum>
}