package eu.vojtechh.redcode.net.red

import eu.vojtechh.redcode.model.red.*
import retrofit2.Response

interface RedApiHelper {
    suspend fun browse(
        auth: String,
        title: String?,
        artist: String?,
        year: String?
    ): Response<RedPageResponse<RedRelease>>

    suspend fun request(
        auth: String,
        title: String?,
        artist: String?
    ): Response<RedPageResponse<RedReleaseRequest>>

    suspend fun groupDetails(
        auth: String,
        groupId: String
    ): Response<RedResponse<RedGroup>>
}