package eu.vojtechh.redcode.net.discogs

import eu.vojtechh.redcode.model.discogs.DGAlbum
import eu.vojtechh.redcode.model.discogs.DGResponse
import retrofit2.Response
import javax.inject.Inject

class DGApiHelperImpl @Inject constructor(
    private val dgApiService: DGApiService
) : DGApiHelper {
    override suspend fun searchBarcode(auth: String, barcode: String): Response<DGResponse> =
        dgApiService.searchBarcode(auth, barcode)

    override suspend fun getMaster(auth: String, masterId: String): Response<DGAlbum> =
        dgApiService.getMaster(auth, masterId)

    override suspend fun getRelease(auth: String, releaseId: String): Response<DGAlbum> =
        dgApiService.getRelease(auth, releaseId)
}