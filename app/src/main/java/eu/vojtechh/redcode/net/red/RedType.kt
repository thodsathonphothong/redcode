package eu.vojtechh.redcode.net.red

enum class RedType {
    RELEASE,
    REQUEST
}