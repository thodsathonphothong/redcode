package eu.vojtechh.redcode

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import coil.load
import coil.transform.CircleCropTransformation
import com.google.zxing.integration.android.IntentIntegrator
import com.kennyc.view.MultiStateView
import dagger.hilt.android.AndroidEntryPoint
import eu.vojtechh.redcode.databinding.ActivityMainBinding
import eu.vojtechh.redcode.model.red.RedGroup
import eu.vojtechh.redcode.model.red.RedReleaseRequest
import eu.vojtechh.redcode.net.red.RedSuccess
import eu.vojtechh.redcode.util.Status
import eu.vojtechh.redcode.vm.MainViewModel


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var model: MainViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProvider(this).get(MainViewModel::class.java)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.buttonScan.setOnClickListener {
            val scan = IntentIntegrator(this).apply {
                setBeepEnabled(false)
                setOrientationLocked(false)
                setPrompt("")
                setBarcodeImageEnabled(false)
            }.createScanIntent()
            scanResult.launch(scan)
        }

        model.barcodeSearchResult.observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    switchState(MultiStateView.ViewState.CONTENT)
                    if (it.data is RedGroup) {
                        binding.textDetails.isVisible = true
                        val group = it.data.group
                        val torrents = it.data.torrents
                        binding.resultText.text = group.name
                        binding.albumArt.isVisible = true
                        binding.albumArt.load(group.wikiImage) {
                            crossfade(true)
                            placeholder(R.drawable.ic_music)
                        }
                        var details =
                            "Artist(s): ${
                                group.musicInfo.artists.joinToString(", ") { a -> a.name }
                            }\nGroup ID: ${group.id}\nYear: ${group.year}\nRecord label: ${group
                                .recordLabel}"
                        details += "\n\n---- Torrents: ----\n\n"
                        details += torrents.map { t ->
                            "Media: ${t.media}\nFormat: ${t.format}\nEncoding: " +
                                    "${t.encoding}\nRemastered: ${t.remastered}\n"
                        }.joinToString("\n")
                        binding.textDetails.text = details
                        binding.buttonViewDetails.setOnClickListener { _ ->
                            openUri("https://redacted.ch/torrents.php?id=${it.data.group.id}")
                        }
                    } else if (it.data is RedSuccess<*> && it.data.data is RedReleaseRequest) {
                        binding.textDetails.isVisible = false
                        binding.albumArt.isVisible = false
                        binding.resultText.text = getString(R.string.found_request)
                        binding.buttonViewDetails.setOnClickListener { _ ->
                            openUri("https://redacted.ch/requests.php?action=view&id=${it.data.data.requestId}")
                        }
                    }
                }
                Status.ERROR -> {
                    switchState(MultiStateView.ViewState.ERROR)
                    binding.multiStateView.getView(MultiStateView.ViewState.ERROR)?.apply {
                        findViewById<TextView>(R.id.textError).text = it.message
                    }
                }
                Status.LOADING -> {
                    switchState(MultiStateView.ViewState.LOADING)
                    binding.multiStateView.getView(MultiStateView.ViewState.LOADING)?.apply {
                        findViewById<TextView>(R.id.textLoading).text = "${it.data.toString()}..."
                    }
                }
            }
        }
    }

    private fun openUri(uri: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        startActivity(browserIntent)
    }

    private fun switchState(newState: MultiStateView.ViewState) {
        if (binding.multiStateView.viewState != newState) {
            binding.multiStateView.viewState = newState
        }
    }

    private fun onScanned(barcode: String) {
        model.searchBarcode(barcode)
    }

    private val scanResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        IntentIntegrator.parseActivityResult(
            0x0000c0de, it.resultCode, it.data
        )?.contents?.let { code ->
            onScanned(code)
        }
    }
}