package eu.vojtechh.redcode

object Constants {
    const val DG_BASE_URL = "https://api.discogs.com/"
    const val RED_BASE_URL = "https://redacted.ch/"
}