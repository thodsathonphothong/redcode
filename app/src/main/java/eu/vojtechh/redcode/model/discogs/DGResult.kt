package eu.vojtechh.redcode.model.discogs

import com.google.gson.annotations.SerializedName

data class DGResult(
    val master_id: String?,
    @SerializedName("id")
    val release_id: String?
)