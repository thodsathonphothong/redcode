package eu.vojtechh.redcode.model.red

data class RedTorrent(
    val media: String,
    val format: String,
    val encoding: String,
    val remastered: String
)
