package eu.vojtechh.redcode.model

data class Album(
    val artistName: String,
    val title: String,
    val year: String
)