package eu.vojtechh.redcode.model.red

data class RedPage<T>(
    var results: List<T>
)