package eu.vojtechh.redcode.model.red

data class RedGroupDetails(
    val name: String,
    val id: String,
    val year: String,
    val recordLabel: String,
    val musicInfo: RedMusicInfo,
    val wikiImage: String,
)
