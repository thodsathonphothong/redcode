package eu.vojtechh.redcode.model.red

data class RedMusicInfo(
    val artists: List<Artist>
)

data class Artist(
    val id: Long,
    val name: String
)