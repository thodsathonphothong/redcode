package eu.vojtechh.redcode.model.red

data class RedResponse<T>(
    val status: String,
    val response: T,
)