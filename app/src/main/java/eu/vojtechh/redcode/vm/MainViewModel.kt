package eu.vojtechh.redcode.vm

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import eu.vojtechh.redcode.repo.DGRepo
import eu.vojtechh.redcode.repo.MainRepo
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepo: MainRepo
) : ViewModel() {
    private val _barcodeSearchResult = MutableLiveData<String>()
    val barcodeSearchResult get() = _barcodeSearchResult.switchMap {
        mainRepo.lookup(it).asLiveData()
    }

    fun searchBarcode(barcode: String) {
        _barcodeSearchResult.value = barcode
    }
}