package eu.vojtechh.redcode.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import eu.vojtechh.redcode.BuildConfig
import eu.vojtechh.redcode.Constants
import eu.vojtechh.redcode.net.discogs.DGApiHelper
import eu.vojtechh.redcode.net.discogs.DGApiHelperImpl
import eu.vojtechh.redcode.net.discogs.DGApiService
import eu.vojtechh.redcode.net.red.RedApiHelper
import eu.vojtechh.redcode.net.red.RedApiHelperImpl
import eu.vojtechh.redcode.net.red.RedApiService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else {
        OkHttpClient
            .Builder()
            .build()
    }

    @Singleton
    @Provides
    @Named("DGRetrofit")
    fun provideDGRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(Constants.DG_BASE_URL)
        .client(okHttpClient)
        .build()

    @Singleton
    @Provides
    @Named("RedRetrofit")
    fun provideRedRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(Constants.RED_BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideDGApiService(@Named("DGRetrofit") retrofit: Retrofit) = retrofit.create(DGApiService::class.java)

    @Provides
    @Singleton
    fun provideDGApiHelper(apiHelper: DGApiHelperImpl): DGApiHelper = apiHelper

    @Provides
    @Singleton
    fun provideRedApiService(@Named("RedRetrofit") retrofit: Retrofit) = retrofit.create(RedApiService::class.java)

    @Provides
    @Singleton
    fun provideRedApiHelper(apiHelper: RedApiHelperImpl): RedApiHelper = apiHelper

}